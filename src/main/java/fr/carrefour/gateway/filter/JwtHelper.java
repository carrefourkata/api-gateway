package fr.carrefour.gateway.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.okta.jwt.AccessTokenVerifier;
import com.okta.jwt.Jwt;
import com.okta.jwt.JwtVerificationException;
import com.okta.jwt.JwtVerifiers;

@Component
public class JwtHelper {

	@Value("${okta_keys}")
	private String oktaKeys;

	public String validateTokenAndGetUser(String token) throws JwtVerificationException {
		AccessTokenVerifier jwtVerifier = JwtVerifiers.accessTokenVerifierBuilder().setIssuer(oktaKeys).build();

		Jwt jwt = jwtVerifier.decode(token);
		return (String) jwt.getClaims().get("sub");

	}

}