package fr.carrefour.gateway.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.okta.jwt.JwtVerificationException;

@Component
public class TokenValidatorFilter extends AbstractGatewayFilterFactory<Object> {
	@Autowired
	private JwtHelper jwtHelper;

	@Override
	public GatewayFilter apply(Object config) {
		return (exchange, chain) -> {
			try {
				// Recupere token des headers 
				String authorizationHeader = exchange.getRequest().getHeaders().getFirst("Authorization");

				// voir si le bearer token existe 
				if (authorizationHeader != null) {
					final String token = authorizationHeader.substring(7, authorizationHeader.length());
					String userMail=jwtHelper.validateTokenAndGetUser(token);
					exchange.mutate()
                    .request(builder -> builder.header("X-User-Email", userMail))
                    .build();
					return chain.filter(exchange);
				} else {
					// Handle invalid token (e.g., return 401 Unauthorized)
					exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
					return exchange.getResponse().setComplete();
				}
			} catch (JwtVerificationException e) {
				e.printStackTrace();
				exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
				return exchange.getResponse().setComplete();
			}

		};
	}

}